#if UNITY_EDITOR
using System.IO;
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class MakeProjectDLL : EditorScript
	{
		public string projectName;
		public string scriptsFolderPath;

		public override void Do ()
		{
			List<DirectoryInfo> foldersRemaining = new List<DirectoryInfo>();
			foldersRemaining.Add(new DirectoryInfo(scriptsFolderPath));
			List<FileInfo> files = new List<FileInfo>();
			while (foldersRemaining.Count > 0)
			{
				DirectoryInfo folder = foldersRemaining[0];
				files.AddRange(folder.GetFiles());
				foldersRemaining.AddRange(folder.GetDirectories());
				foldersRemaining.RemoveAt(0);
			}
			string command = "wsl mcs -target:library -out:'" + projectName + ".dll'";
			for (int i = 0; i < files.Count; i ++)
			{
				FileInfo file = files[i];
				if (file.FullName.EndsWith(".cs"))
					command += " '" + projectName + file.FullName.StartAfter(projectName).Replace("\\", "/") + "'";
			}
			print(command);
			ProcessStartInfo processStartInfo = new ProcessStartInfo();
			processStartInfo.FileName = "cmd";
			processStartInfo.Arguments = command;
			processStartInfo.UseShellExecute = false;
			processStartInfo.RedirectStandardOutput = true;
			processStartInfo.RedirectStandardError = true;
			Process process = new Process();
			process.StartInfo = processStartInfo;
			process.Start();
			string output = process.StandardOutput.ReadToEnd();
			string errors = process.StandardError.ReadToEnd();
			print("Output: " + output);
			print("Errors: " + errors);
		}
	}
}
#else
namespace HeroesOfSlimeWorld
{
	public class MakeProjectDLL : EditorScript
	{
	}
}
#endif