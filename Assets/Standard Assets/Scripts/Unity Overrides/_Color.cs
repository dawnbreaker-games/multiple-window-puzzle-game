using System;
using UnityEngine;

[Serializable]
public struct _Color
{
	public float r;
	public float g;
	public float b;
	public float a;

	public _Color (float r, float g, float b, float a)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public static _Color FromColor (Color color)
	{
		return new _Color(color.r, color.g, color.b, color.a);
	}

	public Color ToColor ()
	{
		return new Color(r, g, b, a);
	}
}