using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Player : Acter, IUpdatable, IDestructable
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public float Hp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public float MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public Rigidbody2D rigid;

		void OnEnable ()
		{
			instance = this;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			HandleMoving ();
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		void HandleMoving ()
		{
			// Move (InputManager.MoveInput);
		}

		void Move (Vector2 move)
		{
			
		}

		public override void Act ()
		{
		}

		public void TakeDamage (float amount)
		{
			Death ();
		}

		public void Death ()
		{
			if (isDead)
				return;
			isDead = true;
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				Player player = Instantiate(GameManager.instance.playerPrefab, GameManager.gameStateGos[GameManager.gameStateGos.Count - 1].GetComponent<Transform>());
				Apply (player);
				return player;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}