using System.Numerics;

public interface IRepresentable
{
    BigIntegerRange ValueRange { get; }
    BigInteger Value { get; }
}