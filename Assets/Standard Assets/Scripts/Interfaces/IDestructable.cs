﻿namespace HeroesOfSlimeWorld
{
	public interface IDestructable
	{
		float Hp { get; set; }
		float MaxHp { get; set; }
		
		void TakeDamage (float amount);
		void Death ();
	}
}