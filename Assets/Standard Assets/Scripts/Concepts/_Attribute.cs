﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class _Attribute : Attribute
{
	public Object targetObject;
	public Type targetType;
	public string targetName;
	public static _Attribute[] instances = new _Attribute[0];

	public virtual void Init (Object targetObject, Type targetType, string targetName)
	{
		this.targetObject = targetObject;
		this.targetType = targetType;
		this.targetName = targetName;
	}

	public virtual void Do ()
	{
	}
}
