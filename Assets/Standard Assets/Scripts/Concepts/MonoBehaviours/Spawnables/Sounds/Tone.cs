using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Tone : Sound
	{
		public float frequency;
		public float sampleRate;
		float phase;

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			phase = 0;
		}

		void OnAudioFilterRead (float[] data, int channels)
		{
			for (int i = 0 ; i < data.Length; i += channels)
			{  
				phase += 2 * Mathf.PI * frequency / sampleRate;
				data[i] = Mathf.Sin(phase);
				if (phase >= 2 * Mathf.PI)
					phase -= 2 * Mathf.PI;
			}
		}
	}
}