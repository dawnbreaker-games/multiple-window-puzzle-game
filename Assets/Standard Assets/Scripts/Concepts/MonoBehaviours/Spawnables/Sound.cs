﻿using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Sound : Spawnable
	{
		public AudioSource audioSource;
		public ObjectPool.DelayedDespawn delayedDespawn;

		public struct SoundEntry
		{
			public Sound sound;
			public ObjectPool.DelayedDespawn delayedDespawn;

			public SoundEntry (Sound sound, ObjectPool.DelayedDespawn delayedDespawn)
			{
				this.sound = sound;
				this.delayedDespawn = delayedDespawn;
			}
		}
	}
}