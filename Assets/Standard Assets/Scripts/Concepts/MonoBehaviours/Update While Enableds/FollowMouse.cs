using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class FollowMouse : UpdateWhileEnabled
	{
		public Transform trs;

		public override void DoUpdate ()
		{
			trs.position = CameraScript.instance.camera.ScreenToWorldPoint((Vector2) InputManager.MousePosition).SetZ(trs.position.z);
		}
	}
}