using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HeroesOfSlimeWorld
{
	public class Acter : Asset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		[HideInInspector]
		public bool isDead;

		public virtual void Act ()
		{
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetPositionOfData ();
			SetRotationOfData ();
			SetActiveOfData ();
			SetLayerOfData ();
			SetEnabledOfData ();
		}

		void SetPositionOfData ()
		{
			_Data.position = _Vector2Int.FromVec2Int(trs.position.ToVec2Int());
		}

		void SetPositionFromData ()
		{
			trs.position = _Data.position.ToVec2Int().ToVec3();
		}

		void SetRotationOfData ()
		{
			_Data.rotation = trs.eulerAngles.z;
		}

		void SetRotationFromData ()
		{
			trs.eulerAngles = Vector3.forward * _Data.rotation;
		}

		void SetActiveOfData ()
		{
			_Data.active = gameObject.activeSelf;
		}

		void SetActiveFromData ()
		{
			gameObject.SetActive(_Data.active);
		}

		void SetLayerOfData ()
		{
			_Data.layer = (uint) gameObject.layer;
		}

		void SetLayerFromData ()
		{
			gameObject.layer = (int) _Data.layer;
		}

		void SetEnabledOfData ()
		{
			_Data.enabled = enabled;
		}

		void SetEnabledFromData ()
		{
			enabled = _Data.enabled;
		}
		
		[Serializable]
		public class Data : Asset.Data
		{
			public _Vector2Int position;
			public float rotation;
			public bool active;
			public uint layer;
			public bool enabled;
			public string justTeleportedFromPortalName;

			public override object MakeAsset ()
			{
				throw new NotImplementedException();
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				Acter acter = (Acter) asset;
				acter.SetPositionFromData ();
				acter.SetRotationFromData ();
				acter.SetActiveFromData ();
				acter.SetLayerFromData ();
				acter.SetEnabledFromData ();
			}
		}
	}
}