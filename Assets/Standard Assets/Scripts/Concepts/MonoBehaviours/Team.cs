using System;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	[Serializable]
	public class Team<T>
	{
		public T representative;
		public Color color;
		public Material material;
		public Team<T> opponent;
		public Team<T>[] opponents = new Team<T>[0];
	}
}