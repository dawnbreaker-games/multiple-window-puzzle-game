using System;
using Extensions;
using UnityEngine;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class BigIntegerRange : Range<BigInteger>
{
	public BigIntegerRange (BigInteger min, BigInteger max) : base (min, max)
	{
	}

	public bool DoesIntersect (BigIntegerRange bigIntRange, bool containsMinAndMax = true)
	{
		if (containsMinAndMax)
			return (min >= bigIntRange.min && min <= bigIntRange.max) || (bigIntRange.min >= min && bigIntRange.min <= max) || (max <= bigIntRange.max && max >= bigIntRange.min) || (bigIntRange.max <= max && bigIntRange.max >= min);
		else
			return (min > bigIntRange.min && min < bigIntRange.max) || (bigIntRange.min > min && bigIntRange.min < max) || (max < bigIntRange.max && max > bigIntRange.min) || (bigIntRange.max < max && bigIntRange.max > min);
	}

	public bool Contains (BigInteger f, bool containsMinAndMax = true)
	{
		return Contains(f, containsMinAndMax, containsMinAndMax);
	}

	public bool Contains (BigInteger f, bool containsMin = true, bool containsMax = true)
	{
		bool greaterThanMin = min < f;
		if (containsMin)
			greaterThanMin |= min == f;
		bool lessThanMax = f < max;
		if (containsMax)
			lessThanMax |= f == max;
		return greaterThanMin && lessThanMax;
	}

	public bool GetIntersectionRange (BigIntegerRange bigIntRange, out BigIntegerRange intersectionRange, bool containsMinAndMax = true)
	{
		intersectionRange = null;
		if (DoesIntersect(bigIntRange, containsMinAndMax))
			intersectionRange = new BigIntegerRange(BigInteger.Max(min, bigIntRange.min), BigInteger.Min(max, bigIntRange.max));
		return intersectionRange != null;
	}
}