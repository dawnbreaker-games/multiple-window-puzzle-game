using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HeroesOfSlimeWorld
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, IAttributeHolder
	{
		public Player playerPrefab;
		// public GameObject[] registeredGos = new GameObject[0];
		// [SaveAndLoadValue]
		// public GameModifier[] gameModifiers = new GameModifier[0];
		// public GameObject menuGo;
		public LayerMask whatIsSolid;
		public LayerMask whatFalls;
		[SaveAndLoadValue]
		public static List<GameState> gameStates = new List<GameState>();
		public static List<GameObject> gameStateGos = new List<GameObject>();
		public static List<uint> diedCountAtTurns = new List<uint>();
		public static uint diedCount;
		// public static Dictionary<string, GameModifier> gameModifierDict = new Dictionary<string, GameModifier>();
		// public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoad;
		// public static bool isQuitting;
		// public static float pausedTime;
		// public static float TimeSinceLevelLoad
		// {
		// 	get
		// 	{
		// 		return Time.timeSinceLevelLoad - pausedTime;
		// 	}
		// }
		[SerializeStaticField]
		public static List<ActerRecording> acterRecordings = new List<ActerRecording>();
		public List<ActerRecording> _acterRecordings = new List<ActerRecording>();
		[SerializeStaticField]
		public static List<ActerRecording> recordingActerRecordings = new List<ActerRecording>();
		public List<ActerRecording> _recordingActerRecordings = new List<ActerRecording>();
		[SerializeStaticField]
		public static List<ActerRecording> playingActerRecordings = new List<ActerRecording>();
		public List<ActerRecording> _playingActerRecordings = new List<ActerRecording>();
		[SerializeStaticField]
		public static List<List<ActerRecording>> acterRecordingsAtTurns = new List<List<ActerRecording>>();
		public List<List<ActerRecording>> _acterRecordingsAtTurns = new List<List<ActerRecording>>();
		[SerializeStaticField]
		public static List<List<ActerRecording>> recordingActerRecordingsAtTurns = new List<List<ActerRecording>>();
		public List<List<ActerRecording>> _recordingActerRecordingsAtTurns = new List<List<ActerRecording>>();		
		[SerializeStaticField]		
		public static List<List<ActerRecording>> playingActerRecordingsAtTurns = new List<List<ActerRecording>>();
		public List<List<ActerRecording>> _playingActerRecordingsAtTurns = new List<List<ActerRecording>>();
		public const int VERSION_NUMBER = 1;
		public const uint HIGHEST_FALL = 100;
		bool previousMenuInput;
		static int currentTurnIndex = 1;
		// [SaveAndLoadValue]
		// static string enabledGosString = "";
		// [SaveAndLoadValue]
		// static string disabledGosString = "";
		// const int FRAME_RATE = -1;
		// const float MAX_DELTA_TIME = 1f / 30f;

		public override void Awake ()
		{
			acterRecordings.Clear();
			recordingActerRecordings.Clear();
			playingActerRecordings.Clear();
			acterRecordingsAtTurns.Clear();
			recordingActerRecordingsAtTurns.Clear();
			playingActerRecordingsAtTurns.Clear();
			playingActerRecordingsAtTurns = _playingActerRecordingsAtTurns;
			recordingActerRecordingsAtTurns = _recordingActerRecordingsAtTurns;
			acterRecordingsAtTurns = _acterRecordingsAtTurns;
			playingActerRecordings = _playingActerRecordings;
			recordingActerRecordings = _recordingActerRecordings;
			acterRecordings = _acterRecordings;
			base.Awake ();
			print(SaveAndLoadManager.DefaultSaveFilePath);
			currentTurnIndex = 1;
			diedCount = 0;
			diedCountAtTurns.Clear();
			gameStateGos = new List<GameObject>(new GameObject[] { Player.Instance.trs.parent.gameObject });
			diedCountAtTurns.Add(0);
			acterRecordingsAtTurns.Add(new List<ActerRecording>());
			recordingActerRecordingsAtTurns.Add(new List<ActerRecording>());
			playingActerRecordingsAtTurns.Add(new List<ActerRecording>());
			SaveAndLoadManager.Instance.Init ();
			if (instance != this)
				return;
			if (PlayerPrefs.GetInt("Version", 0) < VERSION_NUMBER)
			{
				PlayerPrefs.DeleteAll();
				PlayerPrefs.SetInt("Version", VERSION_NUMBER);
			}
			// gameModifierDict.Clear();
			// for (int i = 0; i < gameModifiers.Length; i ++)
			// {
			// 	GameModifier gameModifier = gameModifiers[i];
			// 	gameModifierDict.Add(gameModifier.name, gameModifier);
			// }
			// Application.targetFrameRate = FRAME_RATE;
			Application.wantsToQuit += OnWantToQuit;
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnDestroy ()
		{
			if (instance == this)
				SceneManager.sceneLoaded -= OnSceneLoaded;
		}
		
		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			StopAllCoroutines();
			framesSinceLevelLoad = 0;
			// pausedTime = 0;
		}

		void Update ()
		{
			// if (Time.deltaTime > MAX_DELTA_TIME)
			// {
			// 	print(Time.deltaTime);
			// 	return;
			// }
			InputSystem.Update ();
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			if (Time.deltaTime > 0)
			{
				Physics2D.Simulate(Time.deltaTime);
				Physics2D.SyncTransforms();
			}
			if (ObjectPool.Instance != null && ObjectPool.instance.enabled)
				ObjectPool.instance.DoUpdate ();
			HandleRestart ();
			// HandleMenu ();
			framesSinceLevelLoad ++;
			// if (paused)
			// 	pausedTime += Time.unscaledDeltaTime;
		}

		void HandleRestart ()
		{
			if (Keyboard.current.rKey.wasPressedThisFrame)
				_SceneManager.instance.RestartScene ();
		}

		// void HandleMenu ()
		// {
		// 	bool menuInput = InputManager.MenuInput;
		// 	if (menuGo != null && menuInput && !previousMenuInput)
		// 	{
		// 		Time.timeScale = 0;
		// 		menuGo.SetActive(true);
		// 	}
		// 	previousMenuInput = menuInput;
		// }

		public void Quit ()
		{
			Application.Quit();
#if UNITY_EDITOR
			EditorApplication.isPlaying = false;
#endif
		}

		bool OnWantToQuit ()
		{
			// isQuitting = true;
			// PlayerPrefs.DeleteAll();
			SaveAndLoadManager.instance.Save (SaveAndLoadManager.DefaultSaveFilePath);
			Application.wantsToQuit -= OnWantToQuit;
			return true;
		}

		public void ToggleGameObject (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public void DestroyChildren (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				Destroy(trs.GetChild(i).gameObject);
		}

		public void DestroyChildrenImmediate (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				DestroyImmediate(trs.GetChild(i).gameObject);
		}

		public void SetTimeScale (float timeScale)
		{
			Time.timeScale = timeScale;
		}

		public static void Log (object obj)
		{
			print(obj);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}

		public static void DoOnNextEditorUpdate (Action action)
		{
			EditorApplication.update += () => { Do (action); };
		}

		static void Do (Action action)
		{
			EditorApplication.update -= () => { Do (action); };
			action ();
		}
#endif
		
		// public static bool ModifierExistsAndIsActive (string name)
		// {
		// 	GameModifier gameModifier;
		// 	if (gameModifierDict.TryGetValue(name, out gameModifier))
		// 		return gameModifier.isActive;
		// 	else
		// 		return false;
		// }

		// public static bool ModifierIsActive (string name)
		// {
		// 	return gameModifierDict[name].isActive;
		// }

		// public static bool ModifierExists (string name)
		// {
		// 	return gameModifierDict.ContainsKey(name);
		// }

		// [Serializable]
		// public class GameModifier
		// {
		// 	public string name;
		// 	public bool isActive;
		// }

		[Serializable]
		public class GameState
		{
			public Asset.Data[] assetsDatas = new Asset.Data[0];

			public GameState (Asset.Data[] assetsDatas)
			{
				this.assetsDatas = assetsDatas;
			}
		}
	}
}