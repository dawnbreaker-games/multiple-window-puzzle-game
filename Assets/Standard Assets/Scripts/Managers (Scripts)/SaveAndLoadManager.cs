﻿using System;
using System.IO;
using UnityEngine;
using HeroesOfSlimeWorld;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[ExecuteInEditMode]
public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
{
	public static SaveData saveData = new SaveData();
	public static string DefaultSaveFilePath
	{
		get
		{
			return Application.persistentDataPath + Path.DirectorySeparatorChar + "Save File";
		}
	}
	public static string MostRecentSaveFilePath
	{
		get
		{
			return PlayerPrefs.GetString("Most recent save file path", null);
		}
		set
		{
			PlayerPrefs.SetString("Most recent save file path", value);
		}
	}
	
	public void Init ()
	{
		if (!string.IsNullOrEmpty(MostRecentSaveFilePath))
			LoadMostRecent ();
	}

	void OnAboutToSave ()
	{
		GameManager.gameStates.Clear();
		for (int i = 0; i < GameManager.gameStateGos.Count; i ++)
		{
			GameObject gameStateGo = GameManager.gameStateGos[i];
			Asset[] assets = gameStateGo.GetComponentsInChildren<Asset>();
			Asset.Data[] assetsDatas = new Asset.Data[assets.Length];
			for (int i2 = 0; i2 < assets.Length; i2 ++)
			{
				Asset asset = assets[i2];
				asset.SetData ();
				assetsDatas[i2] = asset._Data;
			}
			GameManager.gameStates.Add(new GameManager.GameState(assetsDatas));
		}
		saveData.gameStates = GameManager.gameStates.ToArray();
		saveData.sceneIndex = (uint) _SceneManager.CurrentScene.buildIndex;
	}
	
	public void Save (string filePath)
	{
		OnAboutToSave ();
		FileStream fileStream = new FileStream(filePath, FileMode.OpenOrCreate);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		binaryFormatter.Serialize(fileStream, saveData);
		fileStream.Close();
		MostRecentSaveFilePath = filePath;
	}
	
	public void LoadMostRecent ()
	{
		Load (MostRecentSaveFilePath);
	}
	
	public void Load (string filePath)
	{
		FileStream fileStream = new FileStream(filePath, FileMode.Open);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
		fileStream.Close();
		OnLoaded ();
		MostRecentSaveFilePath = filePath;
	}

	void OnLoaded ()
	{
		if (_SceneManager.CurrentScene.buildIndex != saveData.sceneIndex)
			_SceneManager.Instance.LoadScene ((int) saveData.sceneIndex);
		GameManager.gameStates = new List<GameManager.GameState>(saveData.gameStates);
		for (int i = 0; i < GameManager.gameStates.Count; i ++)
		{
			GameManager.GameState gameState = GameManager.gameStates[i];
			Asset[] assets = GameManager.gameStateGos[GameManager.gameStateGos.Count - 1].GetComponentsInChildren<Asset>();
			for (int i2 = 0; i2 < assets.Length; i2 ++)
			{
				Asset asset = assets[i2];
				if (asset != null)
				{
					bool foundAssetData = false;
					for (int i3 = 0; i3 < gameState.assetsDatas.Length; i3 ++)
					{
						Asset.Data assetData = gameState.assetsDatas[i3];
						if (assetData.name == asset.name)
						{
							foundAssetData = true;
							break;
						}
					}
					if (!foundAssetData)
						DestroyImmediate(asset.gameObject);
				}
			}
			for (int i2 = 0; i2 < gameState.assetsDatas.Length; i2 ++)
			{
				Asset.Data assetData = gameState.assetsDatas[i2];
				GameObject assetGo = GameObject.Find(assetData.name);
				if (assetGo != null)
				{
					Asset asset = assetGo.GetComponent<Asset>();
					assetData.Apply (asset);
				}
				else
					assetData.MakeAsset ();
			}
		}
	}
	
	[Serializable]
	public struct SaveData
	{
		public GameManager.GameState[] gameStates;
		public uint sceneIndex;

	}
}
