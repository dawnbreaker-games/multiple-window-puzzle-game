using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class EventManager : SingletonUpdateWhileEnabled<EventManager>
	{
		public static List<Event> events = new List<Event>();

		public override void DoUpdate ()
		{
			for (int i = 0; i < events.Count; i ++)
			{
				Event _event = events[i];
				if (Time.timeSinceLevelLoad >= _event.time)
				{
					_event.onEvent (_event.args);
					events.RemoveAt(i);
					i --;
				}
			}
		}

		public struct Event
		{
			public Action<object[]> onEvent;
			public object[] args;
			public float time;

			public Event (Action<object[]> onEvent, object[] args, float time)
			{
				this.onEvent = onEvent;
				this.args = args;
				this.time = time;
			}
		}
	}
}